<link type="text/css" href="/css/pikachoose/bottom.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="/js/jquery.pikachoose.full.js"></script>
<script type="text/javascript" src="/js/application.js"></script>
<script>
	$(document).ready(
		function (){
			$(".pikame").PikaChoose({autoPlay:false,showCaption:false});
		});
</script>


<div class="prefix_2 grid_10">
	<div class="imgDetailContainer">
		<div id="pikachoose"></div>
	</div>
</div>
<div class="prefix_1 grid_9">
	<div class="desc_container">
		<div id="work_text">
			<div class="work_title_en">CHAOS</div>
			<div class="en_desc">It is intended a state of perceptual disorientation to the instability it transmits. Geometric shape shares the fragility of the walls that delimits it and the air captured in its interior.</div>
			<div><img src="/images/hr_small.gif" /></div>
			<div class="work_title_es">CAOS
				<span class="sp_desc">Se propone un estado de desorientación perceptiva ante la inestabilidad que transmite.  La forma geométrica comparte la fragilidad de las paredes que la delimita y el aire capturado en su interior.</span>
			</div>
		</div>
		<div id="work_measure" class="hidden">
			<div class="bottom_text measure_desc">62 x 39 cm</div>
		</div>
	</div>
	<!-- 
	<div class="mirilla">
		<a href="javascript:void(0)"
			onclick="javascript:toggle()">
			<img src="/images/peephole.jpg" />
		</a>
	</div>
	 -->
</div>
<div class="clear"></div>
<div class="grid_23 prefix_1">
	<ul class="hidden pikame" id="thumbnails">
		<li><a href="javascript:toggle()"><img id="principal" src="/images/work/41/1s.jpg" ref="/images/work/41/1.jpg"></a></li>
		<li><img src="/images/work/41/2s.jpg" ref="/images/work/41/2.jpg"></li>
		<li><img src="/images/work/41/3s.jpg" ref="/images/work/41/3.jpg"></li>
		<li><img src="/images/work/41/4s.jpg" ref="/images/work/41/4.jpg"></li>
		<li><img src="/images/work/41/5s.jpg" ref="/images/work/41/5.jpg"></li>
	</ul>
</div>
<div class="clear"></div>

