<link type="text/css" href="/css/pikachoose/bottom.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="/js/jquery.pikachoose.full.js"></script>
<script type="text/javascript" src="/js/application.js"></script>
<script>
	$(document).ready(function() {
		$(".pikame").PikaChoose({
			autoPlay : false,
			showCaption : false
		});
	});
</script>


<div class="grid_10">
	<div class="imgDetailContainer">
		<div id="pikachoose"></div>
	</div>
</div>
<div class="grid_14">
	<div class="desc_container">
		<div id="work_text">
			<div class="work_title_en">SPRING FOREVER</div>
			<div class="en_desc">Ambiguous, diverse and contradictory flower. <br/>Everyday life transfors, its familiarity fades.</div>
			<div><img src="/images/hr_small.gif" /></div>
			<div class="work_title_es">PRIMAVERA POR SIEMPRE
				<span class="sp_desc">Flor ambigua, diversa y contradictoria. La cotidianeidad se transforma, su familiaridad se desvanece.</span>
			</div>
		</div>
		<div id="work_measure" class="hidden">
			<div class="bottom_text measure_desc">30 x 200 x 12 cm</div>
		</div>
	</div>
	<!-- 
	<div class="mirilla">
		<a href="javascript:void(0)"
			onclick="javascript:toggle()">
			<img src="/images/peephole.jpg" />
		</a>
	</div>
	 -->
</div>
<div class="clear"></div>
<div class="grid_23 prefix_1">
	<ul class="hidden pikame" id="thumbnails">
		<li><a href="javascript:toggle()"><img id="principal" src="/images/work/24/1s.jpg" ref="/images/work/24/1.jpg"></a></li>
		<li><img src="/images/work/24/2s.jpg" ref="/images/work/24/2.jpg"></li>
		<li><img src="/images/work/24/3s.jpg" ref="/images/work/24/3.jpg"></li>
		<li><img src="/images/work/24/4s.jpg" ref="/images/work/24/4.jpg"></li>
		<li><img src="/images/work/24/5s.jpg" ref="/images/work/24/5.jpg"></li>
		<li><img src="/images/work/24/6s.jpg" ref="/images/work/24/6.jpg"></li>
	</ul>
</div>
<div class="clear"></div>
