<link type="text/css" href="/css/pikachoose/bottom.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="/js/jquery.pikachoose.full.js"></script>
<script type="text/javascript" src="/js/application.js"></script>
<script>
	$(document).ready(
		function (){
			$(".pikame").PikaChoose({autoPlay:false,showCaption:false});
		});
</script>


<div class="prefix_2 grid_8">
	<div class="imgDetailContainer">
		<div id="pikachoose"></div>
	</div>
</div>
<div class="grid_14">
	<div class="desc_container">
		<div id="work_text">
			<div class="work_title_en">SOUL OF POET</div>
			<div class="en_desc">The exterior is a kind of long skin marked by the vicissitudes of life, full of textures that enrich the quality of what is stored inside. The verses that hoards, the same that struggle to get out turned into butterflies to freedom.</div>
			<div><img src="/images/hr_small.gif" /></div>
			<div class="work_title_es">ALMA DE POETA
				<span class="sp_desc">El exterior es una suerte de larga piel marcada por los avatares de la vida, llena de texturas que enriquecen la calidad de lo que se guarda en su interior. Los versos que atesora, los mismos que pugnan por salir convertidos en mariposas hacia la libertad.</span>
			</div>
		</div>
		<div id="work_measure" class="hidden">
			<div class="bottom_text measure_desc">49 x 32 cm</div>
		</div>
	</div>
	<!-- 
	<div class="mirilla">
		<a href="javascript:void(0)"
			onclick="javascript:toggle()">
			<img src="/images/peephole.jpg" />
		</a>
	</div>
	 -->
</div>
<div class="clear"></div>
<div class="grid_23 prefix_1">
	<ul class="hidden pikame" id="thumbnails">
		<li><a href="javascript:toggle()"><img id="principal" src="/images/work/43/1s.jpg" ref="/images/work/43/1.jpg"></a></li>
		<li><img src="/images/work/43/2s.jpg" ref="/images/work/43/2.jpg"></li>
		<li><img src="/images/work/43/3s.jpg" ref="/images/work/43/3.jpg"></li>
		<li><img src="/images/work/43/4s.jpg" ref="/images/work/43/4.jpg"></li>
		<li><img src="/images/work/43/5s.jpg" ref="/images/work/43/5.jpg"></li>
		<li><img src="/images/work/43/6s.jpg" ref="/images/work/43/6.jpg"></li>
		<li><img src="/images/work/43/7s.jpg" ref="/images/work/43/7.jpg"></li>
		<li><img src="/images/work/43/8s.jpg" ref="/images/work/43/8.jpg"></li>
		<li><img src="/images/work/43/9s.jpg" ref="/images/work/43/9.jpg"></li>
		<li><img src="/images/work/43/10s.jpg" ref="/images/work/43/10.jpg"></li>
		<li><img src="/images/work/43/11s.jpg" ref="/images/work/43/11.jpg"></li>
	</ul>
</div>
<div class="clear"></div>

