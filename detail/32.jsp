<link type="text/css" href="/css/pikachoose/bottom.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="/js/jquery.pikachoose.full.js"></script>
<script type="text/javascript" src="/js/application.js"></script>
<script>
	$(document).ready(
		function (){
			$(".pikame").PikaChoose({autoPlay:false,showCaption:false});
		});
</script>


<div class="prefix_2 grid_9">
	<div class="imgDetailContainer">
		<div id="pikachoose"></div>
	</div>
</div>
<div class="grid_13">
	<div class="desc_container">
		<div id="work_text">
			<div class="work_title_en">FOREST OF FAITH</div>
			<div class="en_desc">The institution is deformed, earthly as it is. The hope that the road unifies. The desire to lower, heaven to earth, sprouting society in virtue.
			<br/>
			The mirror arises as desire to experiment with the landscape, allows visitor to interact with the real and reflected.			
			</div>
			<div><img src="/images/hr_small.gif" /></div>
			<div class="work_title_es">BOSQUE DE FE
				<span class="sp_desc">La institución se deforma, terrenal como es. La esperanza que el camino se unifique. El deseo de bajar el cielo a la tierra, brotando una sociedad en virtud.
El espejo surge como deseo de experimentar con el paisaje, permite al visitante interactuar con lo real y lo reflejado.</span>
			</div>
		</div>
		<div id="work_measure" class="hidden">
			<div class="bottom_text measure_desc">120 x 150 x 150 cm</div>
		</div>
	</div>
	<!-- 
	<div class="mirilla">
		<a href="javascript:void(0)"
			onclick="javascript:toggle()">
			<img src="/images/peephole.jpg" />
		</a>
	</div>
	 -->
</div>
<div class="clear"></div>
<div class="grid_23 prefix_1">
	<ul class="hidden pikame" id="thumbnails">
		<li><a href="javascript:toggle()"><img id="principal" src="/images/work/32/1s.jpg" ref="/images/work/32/1.jpg"></a></li>
		<li><img src="/images/work/32/2s.jpg" ref="/images/work/32/2.jpg"></li>
		<li><img src="/images/work/32/3s.jpg" ref="/images/work/32/3.jpg"></li>
		<li><img src="/images/work/32/4s.jpg" ref="/images/work/32/4.jpg"></li>
		<li><img src="/images/work/32/5s.jpg" ref="/images/work/32/5.jpg"></li>
		<li><img src="/images/work/32/6s.jpg" ref="/images/work/32/6.jpg"></li>
		<li><img src="/images/work/32/7s.jpg" ref="/images/work/32/7.jpg"></li>
		<li><img src="/images/work/32/8s.jpg" ref="/images/work/32/8.jpg"></li>
	</ul>
</div>
<div class="clear"></div>

