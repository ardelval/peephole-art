<link type="text/css" href="/css/pikachoose/bottom.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="/js/jquery.pikachoose.full.js"></script>
<script type="text/javascript" src="/js/application.js"></script>
<script>
	$(document).ready(
		function (){
			$("#bigPicture").hide();
			$(".pikame").PikaChoose({autoPlay:false,showCaption:false});
		});
	
	function hideMeasure(){
		$("#measure").hide();
	}
	function showMeasure(){
		$("#measure").show();
	}	
</script>


<div class="grid_10">&nbsp;
	<div class="imgDetailContainer" id="bigPicture">
		<div id="pikachoose"></div>
	</div>
</div>
<div class="grid_10 sufix_4">
	<div class="desc_container2">
		<div id="work_text">
			<div class="work_title_en">DIALOGUES OF LIFE</div>
			<div class="en_desc">Reflection of personal exchange, through experiences, and hope to overcoming.</div>
			<div><img src="/images/hr_small.gif" /></div>
			<div class="work_title_es">DIALOGOS DE VIDA
				<span class="sp_desc">Reflexi�n del intercambio personal, a trav�s de experiencias vividas, y la esperanza de superaci�n.</span>
			</div>
		</div>
		<div id="work_measure" class="hidden">
			<div id="measure" class="bottom_text measure_desc">30 x 200 x 12 cm</div>
		</div>
	</div>
</div>
<div class="clear"></div>
	<div class="grid_23" id="mainPicture">
		<a href="javascript:toggle()"><img src="/images/work/52/1.jpg"/></a>
	</div>
	<!-- 
	<div class="grid_23 mirilla">
		<a href="javascript:void(0)"
			onclick="javascript:toggle()">
			<img src="/images/peephole.jpg" />
		</a>
	</div>
	 -->
<div class="clear"></div>
<div class="grid_23 prefix_1">
	<ul class="hidden pikame" id="thumbnails">
		<li><a href="javascript:toggle()"><img src="/images/work/52/2s.jpg" ref="/images/work/52/2.jpg" onclick="showMeasure();"></a></li>
		<li><img src="/images/work/52/3s.jpg" ref="/images/work/52/3.jpg" onclick="showMeasure();"></li>
		<li><img src="/images/work/52/4s.jpg" ref="/images/work/52/4.jpg" onclick="showMeasure();"></li>
		<li><img src="/images/work/52/5s.jpg" ref="/images/work/52/5.jpg" onclick="showMeasure();"></li>
		<li><img src="/images/work/52/7s.jpg" ref="/images/work/52/7.jpg" onclick="showMeasure();"></li>
		<li><img src="/images/work/52/8s.jpg" ref="/images/work/52/8.jpg" onclick="showMeasure();"></li>
		<li><img src="/images/work/52/9s.jpg" ref="/images/work/52/9.jpg" onclick="hideMeasure();"></li>
	</ul>
</div>
<div class="clear"></div>

