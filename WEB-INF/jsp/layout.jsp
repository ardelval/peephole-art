<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="/css/main.css" TYPE="text/css"/>
	<link rel="stylesheet" href="/css/960/960_24_col.css" TYPE="text/css"/>
	<title><tiles:insertAttribute name="title" ignore="true" /></title>
</head>
<body>
<div class="container_24">
	<div class="grid_24" id="header">
		<tiles:insertAttribute name="header" />
	</div>
	<div class="clear"></div>
	<div class="main">
		<tiles:insertAttribute name="body" />
	</div>
	<div class="clear"></div>
	<div class="grid_24" id="footer">
		<tiles:insertAttribute name="footer" />
	</div>
</div>
</body>
</html>